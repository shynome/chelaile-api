package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/shynome/chelaile-api/api"
)

func main() {
	var httpPort = ":8080"
	h := api.Register()
	http.Handle("/graphql", h)
	fmt.Printf("server running at %s \n", httpPort)
	log.Fatal(http.ListenAndServe(httpPort, nil))
}
