package chelaile

type chelaileJsonr struct {
	Start    string
	StartInt int
	End      string
}

var (
	jsonStart = []byte("**YGKJ{\"jsonr\":")
	jsonEnd   = []byte("}YGKJ##")
	jsonStartLen = len(jsonStart)
	jsonEndLen   = len(jsonEnd)
	baseURL      = "https://web.chelaile.net.cn/"
	commonParams = map[string]string{
		"s":       "h5",
		"v":       "3.3.19",
		"src":     "webapp_weixin_mp",
		"sign":    "1",
		"gpstype": "wgs",
	}
	commonHeaders = map[string]string{
		"referer": "https://web.chelaile.net.cn/ch5/index.html",
	}
)

