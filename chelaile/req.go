package chelaile

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/imdario/mergo"
)

// ReqOptions 请求的参数
type ReqOptions struct {
	API     string      // 请求的地址
	Params  interface{} // 请求的参数
	Headers interface{} // 请求的头部
}

// Req 接口请求封装
func Req(options ReqOptions) (res map[string]interface{}, err error) {
	req, err := http.NewRequest("GET", baseURL+options.API, nil)
	if err != nil {
		return
	}
	var params, headers map[string]string

	mergo.Merge(&params, commonParams)
	mergo.Merge(&params, options.Params)

	mergo.Merge(&headers, commonHeaders)
	mergo.Merge(&headers, options.Headers)

	// set query params
	query := req.URL.Query()
	for k, v := range params {
		query.Add(k, v)
	}
	req.URL.RawQuery = query.Encode()

	// set header
	for k, v := range headers {
		req.Header.Set(k, v)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if string(body[0:jsonStartLen]) == string(jsonStart) {
		body = body[jsonStartLen : -1*jsonEndLen]
	}

	var jsonResponse map[string]interface{}
	err = json.Unmarshal(body, &jsonResponse)
	if err != nil {
		return
	}

	status := jsonResponse["status"]
	switch status.(type) {
	case string:
		if status != "OK" {
			err = errors.New("response status is not ok. is " + status.(string))
			return
		}
	case int:
		if status != 0 {
			err = errors.New("respose is error. status is " + string(status.(int)))
			return
		}
	default:
		err = errors.New("reponse without status")
		return
	}

	_res, ok := jsonResponse["data"].(map[string]interface{})

	if ok {
		res = _res
	} else {
		err = errors.New("data 不是 json")
	}

	return

}

