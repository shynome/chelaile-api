package api

import (
	"github.com/graphql-go/handler"
	"gitlab.com/shynome/chelaile-api/api/schema"
)

func Register() *handler.Handler {
	h := handler.New(&handler.Config{
		Schema:     &schema.Schema,
		Pretty:     true,
		Playground: true,
	})
	return h
}
