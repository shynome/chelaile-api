package query

import (
	"github.com/graphql-go/graphql"
)

var cityID = &graphql.Field{
	Type:        graphql.String,
	Description: "城市ID",
}

