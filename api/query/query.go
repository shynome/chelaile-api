package query

import (
	"errors"

	"github.com/graphql-go/graphql"
)

// QueryType is QueryType
var QueryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "query",
		Fields: graphql.Fields{
			"hello": &graphql.Field{
				Type: graphql.String,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					return "world", errors.New("555555555")
				},
			},
			"cityList": cityList,
			"line": line,
		},
	},
)
