package query

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/chelaile"
)

// CityType is CityType
var CityType = graphql.NewObject(graphql.ObjectConfig{
	Name: "City",
	Fields: graphql.Fields{
		"cityId":        {Type: graphql.String},
		"cityName":      {Type: graphql.String},
		"cityVersion":   {Type: graphql.String},
		"isGpsCity":     {Type: graphql.Int},
		"isHot":         {Type: graphql.Int},
		"isSupport":     {Type: graphql.Int},
		"pinyin":        {Type: graphql.String},
		"supportSubway": {Type: graphql.Int},
	},
})

// PositionType is PositionType
var PositionType = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "Position",
	Fields: graphql.InputObjectConfigFieldMap{
		"lat": {
			Type:        graphql.Float,
			Description: "纬度",
		},
		"lng": {
			Type:        graphql.Float,
			Description: "经度",
		},
	},
})

var cityList = &graphql.Field{
	Type: graphql.NewList(CityType),
	Args: graphql.FieldConfigArgument{
		"position": {Type: PositionType},
	},
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		res, err := chelaile.Req(chelaile.ReqOptions{
			API:    "wwd/ncitylist",
			Params: p.Args["position"],
		})
		return res["cityList"], err
	},
}
