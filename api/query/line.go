package query

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/chelaile"
	"gitlab.com/shynome/chelaile-api/api/types"
)

// LineResponseType is LineResponse
var LineResponseType = graphql.NewObject(graphql.ObjectConfig{
	Name:        "LineResponse",
	Description: "注意逆向班次的id不是同一个, 是 otherlines 里面的",
	Fields: graphql.Fields{
		"targetOrder": {
			Type:        graphql.Int,
			Description: "目标站点",
		},
		"line": {Type: types.Line},
		"otherlines": {
			Type:        graphql.NewList(graphql.NewNonNull(types.Line)),
			Description: "逆向班次的ID. 可用字段只有: lineId,startSn,endSn,firstTime,lastTime,price ",
		},
		"isGpsCity":     {Type: graphql.Int},
		"isHot":         {Type: graphql.Int},
		"isSupport":     {Type: graphql.Int},
		"pinyin":        {Type: graphql.String},
		"supportSubway": {Type: graphql.Int},
	},
})

// line is Line
var line = &graphql.Field{
	Type: LineResponseType,
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		res, err := chelaile.Req(chelaile.ReqOptions{
			API:    "wwd/ncitylist",
			Params: p.Args["position"],
		})
		cityList := res["cityList"].([]interface{})
		return cityList, err
	},
}
