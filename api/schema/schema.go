package schema

import (
	"fmt"
	"os"

	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/api/query"
)

var Schema graphql.Schema

func init() {
	var err error
	Schema, err = graphql.NewSchema(
		graphql.SchemaConfig{
			Query: query.QueryType,
		},
	)
	if err != nil {
		fmt.Print(err)
		os.Exit(1)
	}
}

