package types

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/api/scalars"
)

// City is City
var City = graphql.NewObject(graphql.ObjectConfig{
	Name: "City",
	Fields: graphql.Fields{
		"cityId":      {Type: CityID},
		"cityName":    {Type: graphql.String},
		"cityVersion": {Type: graphql.String},
		"isGpsCity": {
			Type:        scalars.IntBool,
			Description: "GPS定位的城市",
		},
		"isHot": {
			Type:        scalars.IntBool,
			Description: "是否是热门城市",
		},
		"isSupport": {
			Type:        scalars.IntBool,
			Description: "车来了是否支持该城市",
		},
		"pinyin": {
			Type:        graphql.String,
			Description: "城市名称的拼音. 车来了用来做拼音排序",
		},
		"supportSubway": {
			Type:        scalars.IntBool,
			Description: "该城市是否支持地铁",
		},
	},
})
