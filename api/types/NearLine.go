package types

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/api/scalars"
	"gitlab.com/shynome/chelaile-api/api/types"
)

// NearLine is nearline
var NearLine = graphql.NewObject(graphql.ObjectConfig{
	Name: "NearLine",
	Fields: graphql.Fields{
		"allLineSize": {
			Type: graphql.Int,
		},
		"canDel": {
			Type: scalars.IntBool,
		},
		"distance": {
			Type:        graphql.Int,
			Description: "与当前位置的距离",
		},
		"favsbLines": {Type: graphql.NewList(scalars.Any)},
		"lines": {
			Type: graphql.NewList(graphql.NewNonNull(WaitLine)),
		},
		"sId": {
			Type:        graphql.String,
			Description: "Station Id",
		},
		"sn": {
			Type:        graphql.String,
			Description: "Station Name",
		},
		"sType": {
			Type: graphql.Int,
		},
		"sortPolicy": {
			Type:        graphql.String,
			Description: "不明所以得排序规则",
		},
		"subwayLines": {Type: scalars.Any},
		"tag":         {Type: graphql.String},
	},
})
