package types

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/api/scalars"
)

// Line is Line description
var Line = graphql.NewObject(graphql.ObjectConfig{
	Name:        "Line",
	Description: "公交线路",
	Fields: graphql.Fields{
		"desc": {Type: graphql.String},
		"shortDesc": {
			Type:        graphql.String,
			Description: "简短描述",
		},
		"direction": {
			Type:        scalars.IntBool,
			Description: "0 表示正方向, 1 表示逆方向",
		},
		"endSn": {
			Type:        graphql.String,
			Description: "终点站名字",
		},
		"startSn": {
			Type:        graphql.String,
			Description: "起点站名字",
		},
		"firstTime": {
			Type:        graphql.String,
			Description: "首班车发车时间",
		},
		"lastTime": {
			Type:        graphql.String,
			Description: "末班车发车时间",
		},
		"lineId": {
			Type:        graphql.String,
			Description: "线路ID",
		},
		"name": {
			Type:        graphql.String,
			Description: "线路名字, 第几路",
		},
		"price": {
			Type:        graphql.String,
			Description: "价格",
		},
		"sortPolicy": {
			Type:        graphql.String,
			Description: "排序规则. 值可能是: `flpolicy=0`",
		},
		"state": {
			Type:        scalars.IntBool,
			Description: "0 可用, 1 不可用",
		},
		"stationsNum": {
			Type:        graphql.Int,
			Description: "'应该是车站编号. 只遇到了值: 21",
		},
		"type": {
			Type:        graphql.Int,
			Description: "未发现用处, 目前只遇到了 0 这一个值",
		},
		"version": {
			Type:        graphql.String,
			Description: "像是预留的版本号, 现在我遇到的都是空值",
		},
	},
})
