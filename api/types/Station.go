package types

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/api/scalars"
)

// Station is Station
var Station = graphql.NewObject(graphql.ObjectConfig{
	Name:        "Station",
	Description: "车站信息",
	Fields: graphql.Fields{
		"distanceToSp": {
			Type:        graphql.Int,
			Description: "不清楚用处",
		},
		"lat": {Type: graphql.Float},
		"lng": {Type: graphql.Float},
		"metros": {
			Type:        graphql.NewList(scalars.Any),
			Description: "没找到对应数据",
		},
		"order": {
			Type:        graphql.Int,
			Description: "从开始数起的第几站",
		},
		"sId": {
			Type:        graphql.String,
			Description: "StationId",
		},
		"sn": {
			Type:        graphql.String,
			Description: "Station Name (车站名)",
		},
	},
})
