package types

import (
	"github.com/graphql-go/graphql"
)

// CityID is CityID
var CityID = graphql.NewNonNull(graphql.String)

