package types

import (
	"github.com/graphql-go/graphql"
)

// PositionInput is Position
var PositionInput = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "Position",
	Fields: graphql.InputObjectConfigFieldMap{
		"lat": {
			Type:        graphql.Float,
			Description: "纬度",
		},
		"lng": {
			Type:        graphql.Float,
			Description: "经度",
		},
	},
})
