package types

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/api/scalars"
)

// Bus is Bus
var Bus = graphql.NewObject(graphql.ObjectConfig{
	Name:        "Bus",
	Description: "车辆信息",
	Fields: graphql.Fields{
		"acBus": {
			Type:        scalars.IntBool,
			Description: "该车辆是否处于活动状态",
		},
		"beforeBaseIndex": {
			Type:        graphql.Int,
			Description: "上一次所在站点. -1 表示没有值",
		},
		"beforeLat": {
			Type:        graphql.Float,
			Description: "上一次定位. -1 表示没有值",
		},
		"beforeLng": {
			Type:        graphql.Float,
			Description: "上一次定位. -1 表示没有值",
		},
		"busBaseIndex": {
			Type: graphql.Int,
		},
		"busDesc": {
			Type:        graphql.String,
			Description: "车辆描述",
		},
		"busDescList": {
			Type: graphql.NewList(graphql.String),
		},
		"busId": {
			Type:        graphql.String,
			Description: "车牌号",
		},
		"delay": {
			Type:        graphql.Int,
			Description: "延时, 单位不清楚",
		},
		"distanceToSc": {
			Type: graphql.Int,
		},
		"lng": {
			Type:        graphql.Float,
			Description: "车辆定位. -1 表示没有值",
		},
		"lat": {
			Type:        graphql.Float,
			Description: "车辆定位. -1 表示没有值",
		},
		"licence": {
			Type:        graphql.String,
			Description: "车牌号",
		},
		"link": {
			Type: graphql.String,
		},
		"mTicket": {
			Type: graphql.Int,
		},
		"order": {
			Type:        graphql.Int,
			Description: "该车辆所在站点位置",
		},
		"rType": {
			Type: graphql.Int,
		},
		"shareId": {
			Type: graphql.String,
		},
		"state": {
			Type: graphql.Int,
		},
		"syncTime": {
			Type: graphql.Int,
		},
		"travels": {
			Type: graphql.NewList(graphql.NewNonNull(BusTravel)),
		},
		"userName": {
			Type: graphql.String,
		},
		"userPhoto": {
			Type: graphql.String,
		},
	},
})
