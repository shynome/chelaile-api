package types

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/shynome/chelaile-api/api/scalars"
)

// WaitLine is WaitLine
var WaitLine = graphql.NewObject(graphql.ObjectConfig{
	Name: "WaitLine",
	Fields: graphql.Fields{
		"fav": {
			Type:        scalars.IntBool,
			Description: "是否已收藏",
		},
		"favTagName": {
			Type: graphql.String,
		},
		"line": {
			Type: Line,
		},
		"nextStation": {
			Type:        Station,
			Description: "该公交的下一站",
		},
		"sortPolicy": {
			Type: graphql.String,
		},
		"stnStates": {
			Type: scalars.Any,
		},
		"targetStation": {
			Type:        Station,
			Description: "目标车站",
		},
	},
})
