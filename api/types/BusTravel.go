package types

import (
	"github.com/graphql-go/graphql"
)

// BusTravel is BusTravel
var BusTravel = graphql.NewObject(graphql.ObjectConfig{
	Name:        "BusTravel",
	Description: "车辆行驶信息",
	Fields: graphql.Fields{
		"arrivalTime": {
			Type:        graphql.Int,
			Description: "到达时间",
		},
		"debusCost": {
			Type: graphql.Int,
		},
		"debusTime": {
			Type: graphql.Int,
		},
		"order": {
			Type:        graphql.Int,
			Description: "站点排序",
		},
		"pRate": {
			Type: graphql.Int,
		},
		"travelTime": {
			Type:        graphql.Int,
			Description: "不清楚干嘛的",
		},
	},
})
