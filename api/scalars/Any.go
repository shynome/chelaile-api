package scalars

import (
	"github.com/graphql-go/graphql"
)

// Any is Any
var Any = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "IntBool",
	Description: "0 表示 false, 1 表示 true ",
	Serialize:   func(val interface{}) interface{} { return val },
})
