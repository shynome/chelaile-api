package scalars

import (
	"github.com/graphql-go/graphql"
)

// IntBool is IntBool
var IntBool = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "IntBool",
	Description: "0 表示 false, 1 表示 true ",
	Serialize:   func(val interface{}) interface{} { return val },
})
