module gitlab.com/shynome/chelaile-api

require (
	github.com/graphql-go/graphql v0.7.7
	github.com/graphql-go/handler v0.2.3
	github.com/imdario/mergo v0.3.7
	github.com/kr/pretty v0.1.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
